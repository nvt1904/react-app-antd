import BaseInit from '@base';
import LazyLoading from '@base/components/common/LazyLoading';
import Loading from '@base/components/common/Loading';
import useUI from '@base/hooks/ui/useUI';
import routes, { IRoute } from 'pages/routes';
import React, { Suspense } from 'react';
import { Routes } from 'react-router-dom';
import 'styles/index.less';

interface PropsRenderRoutes {
  routes: IRoute[];
  loading?: React.ReactNode;
  baseUrl?: string;
}
export const RenderRoutes = (props: PropsRenderRoutes) => {
  const { routes, loading, baseUrl } = props;
  const { ui } = useUI();
  return (
    <Routes>
      {routes &&
        routes.length &&
        routes.map((route: IRoute, index: number) => {
          return (
            <route.route
              key={`${baseUrl || ''}${index}`}
              path={`${baseUrl || ''}${route.path}`}
              element={
                <Suspense
                  fallback={
                    loading || (
                      <LazyLoading
                        color={ui.theme.variables && ui.theme.variables['@primary-color']}
                      >
                        <Loading
                          size="3em"
                          color={ui.theme.variables && ui.theme.variables['@primary-color']}
                        />
                      </LazyLoading>
                    )
                  }
                >
                  <route.component />
                </Suspense>
              }
            />
          );
        })}
    </Routes>
  );
};

function App() {
  return (
    <div style={{ height: '100vh' }}>
      <BaseInit>
        <RenderRoutes routes={routes} />
      </BaseInit>
    </div>
  );
}

export default App;

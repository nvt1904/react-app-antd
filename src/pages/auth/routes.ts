import { IRoute } from 'pages/routes';
import { lazy } from 'react';
import { Route } from 'react-router-dom';

export const authPaths = {
  login: '/login',
  all: '*'
};

const authRoutes: IRoute[] = [
  {
    route: Route,
    path: authPaths.login,
    component: lazy(() => import('pages/auth/login'))
  },
  {
    route: Route,
    path: authPaths.all,
    component: lazy(() => import('pages/common/NotFoundPage'))
  }
];

export default authRoutes;

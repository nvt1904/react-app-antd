import { RenderRoutes } from 'App';
import authRoutes from 'pages/auth/routes';

function MasterLayout() {
  return (
    <>
      <RenderRoutes routes={authRoutes} />
    </>
  );
}

export default MasterLayout;

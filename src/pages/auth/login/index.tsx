import { Button, Card, Checkbox, Form, Input } from 'antd';
import useLogin from 'hooks/auth/useLogin';
import ElementCenter from '@base/components/common/ElementCenter';

import { useTranslation } from 'react-i18next';
import withUnSession from '@base/components/hocs/withUnSession';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 }
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 }
};

const LoginPage = () => {
  const { t } = useTranslation();
  const { login } = useLogin();

  const onFinish = (values: any) => {
    login({ email: values.email, password: values.password });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <ElementCenter>
      <Card headStyle={{ textAlign: 'center' }} title={t('login')}>
        <Form
          {...layout}
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item label={t('form:labels.email')} name="email" rules={[{ required: true }]}>
            <Input />
          </Form.Item>

          <Form.Item label={t('form:labels.password')} name="password" rules={[{ required: true }]}>
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout} name="remember" valuePropName="checked">
            <Checkbox>{t('form:labels.remember_me')}</Checkbox>
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              {t('form:labels.login')}
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </ElementCenter>
  );
};

export default withUnSession(LoginPage);

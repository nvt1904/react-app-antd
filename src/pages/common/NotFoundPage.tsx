import ElementCenter from '@base/components/common/ElementCenter';
import { Button } from 'antd';
import { useTranslation } from 'react-i18next';
import Title from 'antd/lib/typography/Title';
import { Link } from 'react-router-dom';
import { HomeOutlined } from '@ant-design/icons';
import { Helmet } from 'react-helmet';

const NotFoundPage = () => {
  const { t } = useTranslation();
  return (
    <ElementCenter>
      <Helmet>
        <title>Not found</title>
      </Helmet>
      <div style={{ textAlign: 'center' }}>
        <Title>404 | Not found</Title>
        <Button type="primary">
          <Link to="/">
            {<HomeOutlined />} {t('back_home')}
          </Link>
        </Button>
      </div>
    </ElementCenter>
  );
};

export default NotFoundPage;

import withSession from '@base/components/hocs/withSession';
import useDocument from '@base/hooks/document/useDocument';
import useInitUiCms from '@base/hooks/ui/useInitUI';
import { Layout } from 'antd';
import { RenderRoutes } from 'App';
import webRoutes from 'pages/cms/routes';
import { Helmet } from 'react-helmet';
import ContentLayout from './layouts/content/ContentLayout';
import HeaderLayout from './layouts/header/HeaderLayout';
import SiderLayout from './layouts/sider/SiderLayout';

function Master() {
  useInitUiCms();
  const { document } = useDocument();
  return (
    <>
      <Helmet>
        <title>{document.title}</title>
      </Helmet>
      <Layout style={{ minHeight: '100%' }}>
        <SiderLayout />
        <Layout style={{ height: '100vh' }}>
          <HeaderLayout />
          <ContentLayout>
            <RenderRoutes routes={webRoutes} />
          </ContentLayout>
        </Layout>
      </Layout>
    </>
  );
}

export default withSession(Master);

import { Button, Card } from 'antd';
import IDatePicker from '@base/components/form/IDatePicker';

function ListPage() {
  return (
    <Card style={{ width: '100%', height: '100%' }}>
      <IDatePicker />
      <Button type="primary">Link</Button>
    </Card>
  );
}

export default ListPage;

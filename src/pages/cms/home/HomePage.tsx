import Block from '@base/components/common/Block';

function HomePage() {
  return <Block title="Home">Home Page</Block>;
}

export default HomePage;

import { MenuFoldOutlined } from '@ant-design/icons';
import { Button, Drawer, Layout } from 'antd';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';
import { SiderProps } from 'antd/lib/layout';
import useUI from '@base/hooks/ui/useUI';
import React, { useEffect } from 'react';
import MenuSider from './MenuSider';

const { Sider } = Layout;

interface Props extends SiderProps {}

function SiderLayout(props: Props) {
  const { ui, toggleSider } = useUI();
  const screens = useBreakpoint();
  useEffect(() => {
    if (screens.sm !== undefined) {
      !screens.xl && toggleSider(false);
    }
  }, [screens]);

  const renderContent = () => {
    return (
      <div style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
        <div
          style={{
            height: `calc(${ui.header?.height} + ${ui.header?.margin})`,
            lineHeight: `calc(${ui.header?.height} + ${ui.header?.margin})`,
            display: 'flex',
            justifyContent: ui.sider?.show ? 'space-between' : 'center',
            padding: ui.sider?.show ? `0 0 0 1rem` : '0'
          }}
        >
          {ui.sider?.show && <div>{process.env.REACT_APP_NAME || 'Nvt1904'}</div>}
          <div>
            <Button
              type="link"
              shape="circle"
              onClick={() => toggleSider()}
              icon={<MenuFoldOutlined rotate={ui.sider?.show ? 0 : 180} />}
            />
          </div>
        </div>
        <MenuSider />
      </div>
    );
  };

  return (
    <>
      {screens.md ? (
        <Sider
          id="sider-bar"
          trigger={null}
          width={ui.sider?.width}
          style={{
            height: '100vh',
            ...(!screens.md ? { position: 'fixed', zIndex: 10 } : {})
          }}
          collapsedWidth={screens.md ? ui.sider?.collapsedWidth : 0}
          collapsed={!ui.sider?.show || false}
          {...props}
        >
          {renderContent()}
        </Sider>
      ) : (
        <Drawer
          placement="left"
          closable={false}
          bodyStyle={{ padding: 0 }}
          visible={ui.sider?.show}
          onClose={() => toggleSider(false)}
        >
          {renderContent()}
        </Drawer>
      )}
    </>
  );
}

export default SiderLayout;

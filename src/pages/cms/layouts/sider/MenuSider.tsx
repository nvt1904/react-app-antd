import { HomeOutlined, TeamOutlined } from '@ant-design/icons';
import useDocument from '@base/hooks/document/useDocument';
import useUI from '@base/hooks/ui/useUI';
import { Grid, Menu } from 'antd';
import { cmsPaths } from 'pages/cms/routes';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useLocation } from 'react-router-dom';

const { useBreakpoint } = Grid;

const { SubMenu, Item } = Menu;
export interface ConfigMenu {
  icon?: React.ReactNode;
  title: string;
  path: string;
  children?: ConfigMenu[];
}

const findOpenKeys = (
  configMenus: ConfigMenu[],
  withOpenKeys: string[] = [],
  menuSelected: ConfigMenu[] = []
): { openKeys: string[]; menuSelected: ConfigMenu[] } => {
  if (!configMenus.length) {
    return { openKeys: withOpenKeys, menuSelected };
  }
  const newWithOpenKeys: string[] = [...withOpenKeys];
  const newConfigMenus: ConfigMenu[] = [];
  const newMenuSelected: ConfigMenu[] = [...menuSelected];
  configMenus.map((configMenu) => {
    if (configMenu.path === window.location.pathname) {
      newMenuSelected.push(configMenu);
    }
    if (configMenu.children?.length) {
      configMenu.children.map((config: ConfigMenu) => {
        newConfigMenus.push(config);
        if (config.path === window.location.pathname) {
          newWithOpenKeys.push(configMenu.path);
        }
        return config;
      });
    }
    return configMenu;
  });
  return findOpenKeys(newConfigMenus, newWithOpenKeys, newMenuSelected);
};

function MenuSider() {
  const { t } = useTranslation();
  const location = useLocation();
  const { setTitle } = useDocument();
  const { toggleSider } = useUI();
  const screens = useBreakpoint();
  const [configMenus] = useState<ConfigMenu[]>([
    {
      icon: <HomeOutlined />,
      title: 'home',
      path: cmsPaths.index
    },
    {
      icon: <TeamOutlined />,
      title: 'user',
      path: '/user'
    }
  ]);
  const [findOpenConfigMenus] = useState(findOpenKeys(configMenus));
  const [openKeys] = useState<string[]>(findOpenConfigMenus.openKeys);
  const [selectKeys] = useState<string[]>([location.pathname]);

  useEffect(() => {
    const firstMenuOpen = findOpenConfigMenus.menuSelected;
    setTitle(t(firstMenuOpen.shift()?.title || ''));
  }, []);

  const renderItem = (configMenu: ConfigMenu) => {
    if (configMenu?.children?.length) {
      return (
        <SubMenu icon={configMenu.icon} key={configMenu.path} title={t(configMenu.title)}>
          {configMenu.children.map((config) => {
            return renderItem(config);
          })}
        </SubMenu>
      );
    } else {
      return (
        <Item
          onClick={() => setTitle(t(configMenu.title))}
          icon={configMenu.icon}
          key={configMenu.path}>
          <Link to={configMenu.path}>{t(configMenu.title)}</Link>
        </Item>
      );
    }
  };

  return (
    <Menu
      className="auto-hidden-scroll"
      style={{
        height: '100%',
        width: '100%',
        overflowY: 'auto',
        overflowX: 'hidden',
        borderRight: 'none'
      }}
      mode="inline"
      defaultSelectedKeys={selectKeys}
      defaultOpenKeys={openKeys}
      onSelect={() => {
        !screens.md && toggleSider(false);
      }}>
      {configMenus.map((configMenu) => renderItem(configMenu))}
    </Menu>
  );
}

export default MenuSider;

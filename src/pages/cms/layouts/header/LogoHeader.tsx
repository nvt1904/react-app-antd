import { GithubOutlined } from '@ant-design/icons';
import ElementCenter from '@base/components/common/ElementCenter';

import { RowProps } from 'antd/lib/row';

interface Props extends RowProps {
  height?: string | number;
}

function LogoHeader(props: Props) {
  return (
    <ElementCenter {...props}>
      <GithubOutlined style={{ fontSize: '3em', height: '100%' }} />
    </ElementCenter>
  );
}

export default LogoHeader;

import { MenuUnfoldOutlined } from '@ant-design/icons';
import ChangeLanguage from '@base/components/locale/ChangeLocale';
import useUI from '@base/hooks/ui/useUI';
import { Button, Col, Grid, Layout, Row, Space } from 'antd';
import AvatarHeader from './AvatarHeader';
import NotificationHeader from './NotificationHeader';
import SearchHeader from './SearchHeader';
import SettingHeader from './SettingHeader';

const { Header } = Layout;
const { useBreakpoint } = Grid;

function HeaderLayout() {
  const { ui, toggleSider } = useUI();
  const screens = useBreakpoint();
  return (
    <Header
      hidden={!ui.header?.show !== undefined ? !ui.header?.show : false}
      id="main-header"
      style={{
        padding: '0em',
        borderRadius: '.3em',
        margin: ui.header?.margin,
        height: `calc(${ui.header?.height} - ${ui.header?.margin})`,
        lineHeight: `calc(${ui.header?.height} - ${ui.header?.margin})`
      }}
    >
      <Row gutter={[8, 0]} justify="space-between" align="middle">
        <Col>
          <Button
            hidden={ui.sider?.show || screens.md}
            type="link"
            icon={<MenuUnfoldOutlined />}
            shape="circle"
            onClick={() => toggleSider()}
          />
        </Col>
        <Col>
          <div style={{ textAlign: 'right' }}>
            <Space>
              <SearchHeader />
              <ChangeLanguage />
              <NotificationHeader />
              <AvatarHeader />
              <SettingHeader />
            </Space>
          </div>
        </Col>
      </Row>
    </Header>
  );
}

export default HeaderLayout;

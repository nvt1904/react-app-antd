import { UserOutlined } from '@ant-design/icons';
import { Button, Dropdown, Menu } from 'antd';
import { useTranslation } from 'react-i18next';
import { setSession } from '@base/utils/session';

function MenuAccount() {
  const { t } = useTranslation();
  return (
    <Menu selectable={false}>
      <Menu.Item key="account">{t('account')}</Menu.Item>
      <Menu.Item key="logout" onClick={() => setSession(null)}>
        {t('logout')}
      </Menu.Item>
    </Menu>
  );
}

function AvatarHeader() {
  return (
    <Dropdown overlay={<MenuAccount />} placement="bottomRight" arrow>
      <Button type="text" shape="circle" icon={<UserOutlined />} />
    </Dropdown>
  );
}

export default AvatarHeader;

import { SettingOutlined } from '@ant-design/icons';
import { Button, Drawer, Row, Space, Switch } from 'antd';
import ChangeLanguage from '@base/components/locale/ChangeLocale';
import CirclePickerCustom from '@base/components/common/CirclePickerCustom';
import useUI from '@base/hooks/ui/useUI';
import React, { useState } from 'react';
import { ColorResult } from 'react-color';
import { useTranslation } from 'react-i18next';

function SettingHeader() {
  const { t } = useTranslation();
  const { ui, setUI } = useUI();
  const [showSetting, setShowSetting] = useState<boolean>(false);
  const toggleSetting = (status?: boolean) => {
    setShowSetting(status !== undefined ? status : !showSetting);
  };
  return (
    <>
      <Button
        onClick={() => toggleSetting()}
        type="text"
        shape="circle"
        icon={<SettingOutlined />}
      />
      <Drawer
        title={t('setting')}
        placement="right"
        onClose={() => toggleSetting()}
        visible={showSetting}
        width="286"
      >
        <Space size={12} direction="vertical" style={{ width: '100%' }}>
          <Row justify="space-between" align="middle">
            {t('language')}
            <ChangeLanguage />
          </Row>
          <Row justify="space-between" align="middle">
            {t('theme:dark_mode')}
            <Switch
              onChange={() => {
                ui.theme.name === 'dark'
                  ? setUI({ theme: { name: 'default' } })
                  : setUI({ theme: { name: 'dark' } });
              }}
              checked={ui.theme.name === 'dark'}
              size="small"
            />
          </Row>
          <Row justify="space-between" align="middle">
            <CirclePickerCustom
              onChange={(color: ColorResult) => {
                setUI({ theme: { variables: { '@primary-color': color.hex } } });
              }}
              color={ui.theme.variables && ui.theme.variables['@primary-color']}
            />
          </Row>
        </Space>
      </Drawer>
    </>
  );
}

export default SettingHeader;

import { Layout } from 'antd';
import useUI from '@base/hooks/ui/useUI';
import BackTop from '@base/components/common/BackTopCustom';

const { Content } = Layout;

interface Props {
  children?: React.ReactNode | React.ReactNodeArray;
}

function ContentLayout(props: Props) {
  const { children } = props;
  const { ui } = useUI();
  return (
    <Content>
      <div
        id="main-content"
        style={{
          overflowY: 'auto',
          height: '100%',
          position: 'relative',
          padding: `0 ${ui.header?.margin}`
        }}
      >
        {children}
        <BackTop
          style={{
            right: '.5rem',
            bottom: '3em'
          }}
          targetId="main-content"
        />
      </div>
    </Content>
  );
}

export default ContentLayout;

import { IRoute } from 'pages/routes';
import { lazy } from 'react';
import { Route } from 'react-router-dom';

export const cmsPaths = {
  index: '/',
  user: '/user',
  all: '*'
};

const webRoutes: IRoute[] = [
  {
    route: Route,
    path: cmsPaths.index,
    component: lazy(() => import('pages/cms/home/HomePage'))
  },
  {
    route: Route,
    path: cmsPaths.user,
    component: lazy(() => import('pages/cms/user/ListPage'))
  },
  {
    route: Route,
    path: cmsPaths.all,
    component: lazy(() => import('pages/common/NotFoundPage'))
  }
];

export default webRoutes;

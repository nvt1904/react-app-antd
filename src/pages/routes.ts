import { authPaths } from 'pages/auth/routes';
import { cmsPaths } from 'pages/cms/routes';
import { ComponentType, lazy, LazyExoticComponent } from 'react';
import { Route } from 'react-router-dom';

export interface IRoute {
  route: any;
  path: string;
  component: LazyExoticComponent<ComponentType<any>>;
  fallback?: React.FC;
}

export const paths = {
  auth: {
    ...authPaths,
    index: '/auth/*'
  },
  cms: {
    ...cmsPaths,
    index: '/*'
  }
};

const routes: IRoute[] = [
  {
    route: Route,
    path: paths.auth.index,
    component: lazy(() => import('pages/auth'))
  },
  {
    route: Route,
    path: paths.cms.index,
    component: lazy(() => import('pages/cms'))
  }
];

export default routes;

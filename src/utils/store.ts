import { configureStore } from '@reduxjs/toolkit';
import baseReducer from '@base/hooks/baseReducer';
import profile from 'hooks/auth/reducer';
import session from 'hooks/auth/session/reducer';
import { createStateSyncMiddleware, initMessageListener } from 'redux-state-sync';

const rootReducer = {
  ...baseReducer,
  session,
  profile
};

const store = configureStore({
  reducer: rootReducer,
  middleware: [
    createStateSyncMiddleware({
      whitelist: [
        'ui/changeUI',
        'locale/changeLocale',
        'session/changeSession',
        'profile/changeProfile'
      ]
    })
  ]
});
initMessageListener(store);

export type AppState = ReturnType<typeof store.getState>;
export default store;

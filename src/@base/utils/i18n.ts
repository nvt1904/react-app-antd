import i18n from 'i18next';
import 'moment/min/locales.min';
import languageDetector from 'i18next-browser-languagedetector';
import backend from 'i18next-http-backend';
import { initReactI18next } from 'react-i18next';

i18n
  .use(backend)
  .use(languageDetector)
  .use(initReactI18next)
  .init({
    react: {
      useSuspense: false
    },
    backend: {
      allowMultiLoading: false
    },
    ns: ['translations', 'messages', 'form', 'theme'],
    defaultNS: 'translations',
    fallbackLng: process.env.REACT_APP_LOCALE_DEFAULT || 'vi',
    interpolation: {
      escapeValue: false
    }
  });

export default i18n;

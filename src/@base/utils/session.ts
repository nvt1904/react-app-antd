import { subject } from '@base/hooks/rxjs/useInitSubject';
import { merge } from 'lodash';
import moment from 'moment';
import localStorageHelper, { KeyStorage } from './localStorage';

export type Session = {
  accessToken: string;
  refreshToken: string;
  expires: string;
};

export const setSession = (session: Session | null) => {
  localStorageHelper.setObject(KeyStorage.SESSION, session);
  subject.next({
    type: 'setSession',
    data: session
  });
};

export const refreshToken = async () => {
  return {};
};

export const getSession = async () => {
  try {
    const session = localStorageHelper.getObject<Session | null>(KeyStorage.SESSION, null);
    if (session) {
      if (moment(session?.expires) > moment().add(1, 'minute')) {
        const result = await refreshToken();
        if (result) {
          const newSession: Session = merge(session, result);
          setSession(newSession);
          return newSession;
        }
      }
      return session as Session;
    }
  } catch (error) {}
  setSession(null);
  return null;
};

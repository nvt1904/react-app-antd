import useInitLocale from './locale/useInitLocale';
import useInitSubject from './rxjs/useInitSubject';
import useInitSession from '../../hooks/auth/session/useInitSession';
import useInitUI from './ui/useInitUI';

export default function useInitBase() {
  useInitSubject();
  useInitSession();
  useInitUI();
  useInitLocale();
}

import { useEffect } from 'react';
import { Subject } from 'rxjs';

export const subject = new Subject();

export default function useInitSubject() {
  useEffect(() => {
    const subscribe = subject.subscribe((x) => console.log('RXJS subscribe', x));
    return () => subscribe.unsubscribe();
  }, []);
}

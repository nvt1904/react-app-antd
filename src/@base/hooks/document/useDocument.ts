import { useDispatch, useSelector } from 'react-redux';
import { changeDocument } from '@base/hooks/document/reducer';
import { AppState } from 'utils/store';

function useDocument() {
  const document = useSelector((state: AppState) => state.document);
  const dispatch = useDispatch();
  const setTitle = (title: string) => {
    if (title && document.title !== title) {
      const action = changeDocument({ title });
      dispatch(action);
    }
  };
  return { document, setTitle };
}

export default useDocument;

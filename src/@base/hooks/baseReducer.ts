import document from './document/reducer';
import locale from './locale/reducer';
import ui from './ui/reducer';

const baseReducer = { document, locale, ui };

export default baseReducer;

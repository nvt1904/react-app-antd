import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Locale } from 'antd/es/locale-provider';
import en from 'antd/es/locale/en_US';
import vi from 'antd/es/locale/vi_VN';
import localStorageHelper, { KeyStorage } from '@base/utils/localStorage';
import _ from 'lodash';

export const locales = {
  vi: { locale: vi, name: 'Tiếng Việt' },
  en: { locale: en, name: 'English' }
};

const defaultLocaleName = 'vi';

const initialState: Locale =
  localStorageHelper.getObject(KeyStorage.LOCALE, null) || locales[defaultLocaleName].locale;

const locale = createSlice({
  name: 'locale',
  initialState: initialState,
  reducers: {
    changeLocale: (state: Locale, action: PayloadAction<Locale>) => {
      state = _.merge(state, action.payload);
    }
  }
});

const { reducer, actions } = locale;
export const { changeLocale } = actions;
export default reducer;

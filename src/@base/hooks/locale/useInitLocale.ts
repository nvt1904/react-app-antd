import localStorageHelper, { KeyStorage } from '@base/utils/localStorage';
import i18n from 'i18next';
import moment from 'moment';
import { useEffect } from 'react';
import useLocale from './useLocale';

function useInitLocale() {
  const { locale } = useLocale();

  useEffect(() => {
    moment.locale(locale.locale);
    i18n.changeLanguage(locale.locale);
    window.document.body.dataset.locale = locale.locale;
    localStorageHelper.setObject(KeyStorage.LOCALE, {
      ...locale
    });
  }, [locale]);
}

export default useInitLocale;

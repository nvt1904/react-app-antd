import localStorageHelper, { KeyStorage } from '@base/utils/localStorage';
import { useEffect } from 'react';
import themes from 'themes';
import useUI from './useUI';

function useInitUI(): void {
  const { ui } = useUI();
  useEffect(() => {
    localStorageHelper.setObject(KeyStorage.UI, ui);
  }, [ui]);

  useEffect(() => {
    const newVariables = { ...ui.theme.variables };
    const configTheme = {
      ...themes[ui.theme.name],
      ...newVariables
    };
    global.less
      .modifyVars(configTheme)
      .then(() => {
        window.document.body.dataset.theme = ui.theme.name;
      })
      .catch((err) => console.log(err));
  }, [ui.theme]);
}

export default useInitUI;

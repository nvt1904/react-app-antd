import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import localStorageHelper, { KeyStorage } from '@base/utils/localStorage';
import _ from 'lodash';
import variables from 'themes/variables';

export interface ThemeState {
  name: 'default' | 'dark';
  variables: {
    [key: string]: string;
  };
}

export interface UIState<T = any> {
  header?: {
    margin: string;
    height?: string;
    show?: boolean;
  };
  sider?: {
    show?: boolean;
    width?: string;
    collapsedWidth?: string;
  };
  theme: T;
}

const themeLocal = localStorageHelper.getObject<ThemeState>(KeyStorage.THEME, {
  name: 'default',
  variables: {}
});

const initialState: UIState<ThemeState> = _.merge(
  {
    header: {
      height: '4em',
      margin: '.5rem',
      show: true
    },
    sider: {
      show: true,
      width: '250px',
      collapsedWidth: '50px'
    },
    theme: {
      name: themeLocal.name || 'default',
      variables: Object.keys(themeLocal.variables || {}).length
        ? themeLocal.variables
        : { '@primary-color': variables['@primary-color'] }
    }
  },
  localStorageHelper.getObject(KeyStorage.UI, null) || {}
);

const ui = createSlice({
  name: 'ui',
  initialState: initialState,
  reducers: {
    changeUI: (state: UIState, action: PayloadAction<Partial<UIState<Partial<ThemeState>>>>) => {
      state = _.merge(state, action.payload);
    }
  }
});

const { reducer, actions } = ui;
export const { changeUI } = actions;
export default reducer;

import { AppState } from 'utils/store';
import { useDispatch, useSelector } from 'react-redux';
import { changeUI, UIState } from './reducer';

function useUI() {
  const ui = useSelector((state: AppState) => state.ui);
  const dispatch = useDispatch();
  const setUI = (newUI: Partial<UIState>) => {
    dispatch(changeUI(newUI));
  };

  const toggleSider = (show?: boolean) => {
    setUI({ sider: { show: show !== undefined ? show : !ui?.sider?.show } });
  };

  return { ui, setUI, toggleSider };
}

export default useUI;

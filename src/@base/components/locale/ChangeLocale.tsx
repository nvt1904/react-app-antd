import { TranslationOutlined } from '@ant-design/icons';
import { Select } from 'antd';
import useLocale, { LocaleName } from '@base/hooks/locale/useLocale';

function ChangeLocale() {
  const { locale, langs, setLocale } = useLocale();
  return (
    <Select
      options={langs}
      value={locale.locale}
      onChange={(value) => setLocale(value as LocaleName)}
      suffixIcon={<TranslationOutlined />}
    />
  );
}

export default ChangeLocale;

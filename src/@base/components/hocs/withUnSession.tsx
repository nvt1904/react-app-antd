import useSession from 'hooks/auth/session/useSession';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import Loading from '../common/Loading';

const withUnSession = (WrapperComponent: any) => (props: any) => {
  const navigate = useNavigate();
  const { session } = useSession();

  useEffect(() => {
    if (session) {
      navigate('/');
    }
  }, [session]);

  if (!session) {
    return <WrapperComponent {...props} />;
  }
  return <Loading />;
};

export default withUnSession;

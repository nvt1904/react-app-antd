import useSession from 'hooks/auth/session/useSession';
import { stringifyUrl } from 'query-string';
import { useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import Loading from '../common/Loading';

const withSession = (WrapperComponent: any) => (props: any) => {
  const navigate = useNavigate();
  const location = useLocation();
  const { session } = useSession();

  useEffect(() => {
    if (!session) {
      navigate(
        stringifyUrl({
          url: '/auth/login',
          query: { callbackUrl: location.pathname }
        })
      );
    }
  }, [session]);

  if (session) {
    return <WrapperComponent {...props} />;
  }
  return <Loading />;
};

export default withSession;

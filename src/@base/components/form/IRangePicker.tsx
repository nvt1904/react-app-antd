import { DatePicker } from 'antd';
import { RangePickerProps } from 'antd/es/date-picker/generatePicker';
import useLocale from '@base/hooks/locale/useLocale';
import { Moment } from 'moment';

const { RangePicker } = DatePicker;

function IRangePicker(props: RangePickerProps<Moment>) {
  const { locale } = useLocale();
  return <RangePicker format={locale.DatePicker?.lang.dateFormat} {...props} />;
}

export default IRangePicker;

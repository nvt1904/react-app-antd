import { FormatPainterFilled } from '@ant-design/icons';
import { Button, Card, Dropdown, Menu } from 'antd';
import useUI from '@base/hooks/ui/useUI';
import { CSSProperties } from 'react';
import { ColorResult } from 'react-color';
import CirclePickerCustom from '../common/CirclePickerCustom';

type Props = {
  style?: CSSProperties;
};

function ChangeTheme(props: Props) {
  const { style } = props;
  const { ui, setUI } = useUI();
  const menuTheme = () => {
    return (
      <Menu selectable={false} style={{ padding: 0 }}>
        <Menu.Item style={{ padding: 0 }}>
          <Card>
            <CirclePickerCustom
              onChange={(color: ColorResult) => {
                setUI({ theme: { variables: { '@primary-color': color.hex } } });
              }}
              color={ui.theme.variables && ui.theme.variables['@primary-color']}
            />
          </Card>
        </Menu.Item>
      </Menu>
    );
  };

  return (
    <div
      style={{
        position: 'fixed',
        bottom: '1rem',
        right: '1rem',
        zIndex: 100,
        ...style
      }}
    >
      <Dropdown overlay={menuTheme()} trigger={['contextMenu']} placement="topRight">
        <Button
          onClick={() => {
            if (ui.theme.name === 'dark') {
              setUI({ theme: { name: 'default' } });
            } else {
              setUI({ theme: { name: 'dark' } });
            }
          }}
          shape="circle"
          type="link"
          icon={<FormatPainterFilled />}
        />
      </Dropdown>
    </div>
  );
}

export default ChangeTheme;

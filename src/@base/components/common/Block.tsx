import { ArrowLeftOutlined } from '@ant-design/icons';
import { Button, Card, Col, Row } from 'antd';
import { useNavigate } from 'react-router-dom';

type BlockProps = {
  title: JSX.Element | string;
  onBack?: boolean;
  children: JSX.Element | React.ReactNode | React.ReactNodeArray;
  actions?: JSX.Element | React.ReactNode | React.ReactNodeArray;
};

function Block(props: BlockProps) {
  const { title, onBack, children, actions } = props;
  const navigate = useNavigate();
  const handleOnBack = () => {
    onBack && navigate(-1);
  };
  const btnOnBack = () => {
    if (onBack) {
      return (
        <Button shape="circle" onClick={handleOnBack} type="text">
          <ArrowLeftOutlined />
        </Button>
      );
    } else {
      return undefined;
    }
  };
  return (
    <Card
      title={
        <Row style={{ padding: '0 .5rem' }} gutter={[8, 0]} justify="space-between">
          <Col>
            {btnOnBack()}
            {title}
          </Col>
          <Col>{actions}</Col>
        </Row>
      }
      bordered={false}
      style={{ width: '100%' }}
      headStyle={{ padding: '0' }}
      bodyStyle={{ padding: '1rem .5rem' }}
    >
      {children}
    </Card>
  );
}

export default Block;

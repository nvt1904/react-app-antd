import { FullscreenExitOutlined, FullscreenOutlined } from '@ant-design/icons';
import { useFullscreen } from 'ahooks';
import { Button } from 'antd';
import { ButtonProps } from 'antd/lib/button';

interface Props extends ButtonProps {
  targetId?: string;
}

function ToggleFullScreen(props: Props) {
  const { targetId, ...reset } = props;
  const [isFullscreen, { toggleFullscreen }] = useFullscreen(() =>
    targetId ? window.document.getElementById(targetId) : window.document.body
  );

  return (
    <Button
      onClick={() => toggleFullscreen()}
      type="text"
      icon={isFullscreen ? <FullscreenExitOutlined /> : <FullscreenOutlined />}
      shape={'circle'}
      {...reset}
    />
  );
}

export default ToggleFullScreen;

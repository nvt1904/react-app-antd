import { Loading3QuartersOutlined } from '@ant-design/icons';
import ElementCenter from '@base/components/common/ElementCenter';

interface Props {
  loading?: boolean;
  size?: number | string;
  color?: string;
}

const Loading = (props: Props) => {
  const { loading, size, color } = props;
  return (
    <ElementCenter>
      {loading !== false && (
        <Loading3QuartersOutlined spin style={{ fontSize: size || '100%', color }} />
      )}
    </ElementCenter>
  );
};

export default Loading;

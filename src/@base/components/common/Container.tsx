import React, { CSSProperties } from 'react';

interface Props {
  children: React.ReactNode | React.ReactNodeArray;
  style?: CSSProperties;
}

function Container(props: Props) {
  const { children, style } = props;
  return (
    <div style={{ padding: 0, ...style }} className="container">
      {children}
    </div>
  );
}

export default Container;

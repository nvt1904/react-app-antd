import ChangeTheme from '@base/components/ui/ChangeTheme';
import useLocale from '@base/hooks/locale/useLocale';
import useInitBase from '@base/hooks/useInitBase';
import { ConfigProvider } from 'antd';
import { useTranslation } from 'react-i18next';

type Props = {
  children?: JSX.Element;
};

function BaseWarperInit(props: Props) {
  useInitBase();
  return (
    <>
      {props.children}
      <ChangeTheme />
    </>
  );
}

export default function BaseInit(props: Props) {
  const { t } = useTranslation();
  const { locale } = useLocale();
  return (
    <ConfigProvider
      locale={locale}
      form={{
        validateMessages: t('form:validate_messages', { returnObjects: true })
      }}
    >
      <BaseWarperInit>{props.children}</BaseWarperInit>
    </ConfigProvider>
  );
}

import { useEffect } from 'react';
import firebase from 'utils/firebase';
import { notification } from 'antd';
import { getMessaging, getToken, onMessage } from 'firebase/messaging';

function useInitFirebase() {
  useEffect(() => {
    const messaging = getMessaging(firebase);
    getToken(messaging)
      .then((currentToken) => {
        if (currentToken) {
          onMessage(messaging, (payload) => {
            notification.open({
              message: payload.notification?.title,
              description: payload.notification?.body
            });
          });
          console.log('FCM token =>', currentToken);
        }
      })
      .catch((err) => {});
  }, []);
}

export default useInitFirebase;

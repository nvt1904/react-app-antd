import { useEffect, useRef } from 'react';
import SocketIOClient, { Socket } from 'socket.io-client';

export const socketIOClient = (token: string = '') =>
  SocketIOClient(process.env.NEXT_PUBLIC_SERVER_SOCKET_IO || 'ws://localhost:4000', {
    transports: ['polling'],
    query: { token }
  });

function useInitSocketIO() {
  const ioClient = useRef<Socket>();
  useEffect(() => {
    ioClient.current && ioClient.current.disconnect();
    ioClient.current = socketIOClient('abc');
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    ioClient.current.on('message', (data: any) => {
      console.log(data);
    });
    return () => {
      ioClient.current && ioClient.current.disconnect();
    };
  }, []);
}

export default useInitSocketIO;

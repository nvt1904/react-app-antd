import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export enum Status {
  ACTIVE = 'active',
  INACTIVE = 'inactive'
}

export type Profile = {
  id: number;
  avatar?: number;
  name: string;
  email: string;
  status: Status;
};

const initialState: Profile | Record<string, unknown> = {};

const session = createSlice({
  name: 'session',
  initialState: initialState,
  reducers: {
    changeProfile: (state: Profile | {}, action: PayloadAction<Profile | null>) => {
      return action.payload || {};
    }
  }
});

const { reducer, actions } = session;
export const { changeProfile } = actions;
export default reducer;

import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import localStorageHelper, { KeyStorage } from '@base/utils/localStorage';
import { Session } from '@base/utils/session';

const initialState = localStorageHelper.getObject<Session | Record<string, unknown>>(
  KeyStorage.SESSION,
  {}
);

const session = createSlice({
  name: 'session',
  initialState: initialState,
  reducers: {
    changeSession: (state: Session | {}, action: PayloadAction<Session | null>) => {
      localStorageHelper.setObject(KeyStorage.SESSION, action.payload);
      return action.payload || {};
    }
  }
});

const { reducer, actions } = session;
export const { changeSession } = actions;
export default reducer;

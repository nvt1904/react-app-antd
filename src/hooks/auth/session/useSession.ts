import { useSelector } from 'react-redux';
import { getSession, setSession } from '@base/utils/session';
import { AppState } from 'utils/store';

export default function useSession() {
  const session = useSelector((appState: AppState) =>
    appState.session && appState.session.accessToken ? appState.session : null
  );
  return { session, setSession, getSession };
}

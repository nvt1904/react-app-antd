import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { subject } from '../../../@base/hooks/rxjs/useInitSubject';
import { changeSession } from './reducer';
import useSession from './useSession';

export default function useInitSession() {
  const { session, getSession } = useSession();
  const dispatch = useDispatch();
  useEffect(() => {
    const subscribe = subject.subscribe((event: any) => {
      const { type, data } = event;
      if (type === 'setSession') {
        if (data !== session) {
          dispatch(changeSession(data));
        }
      }
    });
    return () => subscribe.unsubscribe();
  }, []);

  useEffect(() => {
    getSession();
  }, []);
}

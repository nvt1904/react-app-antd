import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axiosClient from '@base/utils/axiosClient';
import { AppState } from 'utils/store';
import { changeProfile, Profile } from './reducer';

function useProfile() {
  const profile = useSelector((state: AppState) =>
    state.profile && state.profile.id ? state.profile : null
  );
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const getProfile = async () => {
    setLoading(true);
    const res = await axiosClient.get<null, Profile>('/auth/profile');
    if (res) {
      dispatch(changeProfile(res));
    }
    setLoading(false);
    return res;
  };

  return {
    loading,
    getProfile,
    profile
  };
}

export default useProfile;

import { Res } from '@base/utils/axiosClient';
import { Session, setSession } from '@base/utils/session';
import { message } from 'antd';
import { useState } from 'react';

export type DataLogin = {
  email: string;
  password: string;
  redirect?: boolean;
  callbackUrl?: string;
};

function useLogin() {
  const [pending, setPending] = useState(false);
  const [result, setResult] = useState<Session>();
  const login = async (data: DataLogin) => {
    setPending(true);
    // const res = await axiosClient.post<Res<Session>>('/auth/login', data);
    const res: Res = await new Promise((resolve) => {
      setTimeout(() => {
        resolve({
          accessToken: 'token'
        });
      }, 1000);
    });
    if (!res.error) {
      setSession(res);
    } else {
      message.error(res.error);
      setSession(null);
    }
    setResult(res);
    setPending(false);
    return res;
  };

  return {
    pending,
    login,
    result
  };
}

export default useLogin;

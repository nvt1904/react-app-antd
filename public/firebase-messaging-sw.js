// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.

// eslint-disable-next-line no-undef
importScripts('https://www.gstatic.com/firebasejs/8.1.2/firebase-app.js');
// eslint-disable-next-line no-undef
importScripts('https://www.gstatic.com/firebasejs/8.1.2/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object

// const firebaseConfig = {
//   apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
//   authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
//   databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
//   projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
//   storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
//   messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
//   appId: process.env.REACT_APP_FIREBASE_APP_ID,
//   measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID
// };
// eslint-disable-next-line no-undef
if (!firebase.apps.length) {
  // eslint-disable-next-line no-undef
  // firebase.initializeApp(firebaseConfig);
  // eslint-disable-next-line no-undef
  firebase.initializeApp({
    apiKey: "AIzaSyAIgSWTXTgxcfBe0syq_Uq2gb7trx66znc",
    authDomain: "base-project-beaf8.firebaseapp.com",
    projectId: "base-project-beaf8",
    storageBucket: "base-project-beaf8.appspot.com",
    messagingSenderId: "766643447982",
    appId: "1:766643447982:web:989afb5007ad8fce7ca53f",
    measurementId: "G-H6W9CBF61P"
  });
}

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.

// eslint-disable-next-line no-undef
const messaging = firebase.messaging();

messaging.onBackgroundMessage(function (payload) {
  // Customize notification here
  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
    icon: '/firebase-logo.png'
  };

  // eslint-disable-next-line no-restricted-globals
  return self.registration.showNotification(notificationTitle, notificationOptions);
});
